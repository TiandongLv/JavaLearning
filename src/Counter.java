    import java.util.InputMismatchException;
    import java.util.Scanner;

    public class Counter {
        public static void main(String[] args){

            Scanner scanner = new Scanner(System.in);

            while (true){
                System.out.println("请输入y开始，输入n退出");
                String a = scanner.next();

                if (a.equals("y")){
                    System.out.println("输入第一个数：");
                    try {
                    float num1 = scanner.nextFloat();
                    System.out.println("输入运算符：");
                    String operator = scanner.next();
                    System.out.println("输入第二个数：");
                    float num2 = scanner.nextFloat();

                    switch (operator){
                        case "+":
                            add(num1,num2);
                            break;
                        case "-":
                            substract(num1,num2);
                            break;
                        case "*":
                            multiply(num1,num2);
                            break;
                        case "/":
                            divide(num1,num2);

                            break;

                    }
                    }
                    catch (InputMismatchException e){
                        System.out.println("输入类型有误");
                        break;
                    }

                }else {
                    System.out.println("退出");
                    break;
                }

            }
            scanner.close();

        }

        public static void add(float a,float b){
            System.out.println("a+b="+(a+b));
        }
        public static void substract(float a,float b){
            System.out.println("a-b="+(a-b));
        }
        public static void multiply(float a,float b){
            System.out.println("a*b="+(a*b));
        }
        public static void divide(float a,float b){
            if ((a/b)== Float.POSITIVE_INFINITY){
                System.out.println("除数不能为0");
            }
            else {
                System.out.println("a/b=" + (a / b));
            }

        }
    }




